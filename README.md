This repository provides a Remote Execution service.

Any client supporting the Remote Execution API (https://github.com/bazelbuild/remote-apis)
should be able to use it.

Check you client documentation to know how to configure it to use Remote Execution

Entry points:

- Prod:
    - frontend: 178.128.143.248:8980
    - event-service (optional): 206.189.242.248:8983
- Staging:
    - frontend: 188.166.203.246:8980
    - event-service (optional): 134.209.136.144:8983

If for some reason those changes (they should not, they are k8s load balancers),
you can take a look to the output of the CI to get the current values.
For example for Prod:
https://gitlab.codethink.co.uk/codethink/infrastructure/k8s-deployments/-/jobs/53098
